function getLastCarDetails(carList) {
  var lenOfCarList = carList.length;

  //check if the dataList is empty
  if (lenOfCarList <= 0 || carList == null) {
    return "No car data exists.";
  }

  //accumulator accumate the vaules or kind of take calulation of prev value, the currentElement is the element which we are on.
  const car = carList.reduce((accumulator, currentElement) => {
    return currentElement;
  });
  return "Last car is a " + car.car_make + " " + car.car_model + "."; //pringting last car data
}

module.exports = getLastCarDetails;
