function getCarDetailById(carList, carId) {
  if (carId == null || carId < 0) {
    return "Provide some valid input";
  }
  let car = carList.find((elementAtIndex) => elementAtIndex.id == carId); // getting  data of that car by carId

  //using condition printing if data present and if not then error.
  if (car) {
    return (
      "Car " +
      car.id +
      " is a " +
      car.car_year +
      " " +
      car.car_make +
      " " +
      car.car_model
    );
  } else {
    return "Car with id " + carId + "not found";
  }
}

module.exports = getCarDetailById;
