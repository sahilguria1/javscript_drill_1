function allCarDetailsJson(carList) {
  const lenCarList = carList.length;
  //handling edge cases
  if (carList == null || lenCarList <= 0) {
    return "No element of car exists.";
  }
  const car = carList.filter(
    (car) => car.car_make == "BMW" || car.car_make == "Audi"
  );
  return JSON.stringify(car, null, 10); // printing all the cars detail
}

module.exports = allCarDetailsJson;
