//method get car older than gven year
function getCarOlderByYear(carList, year) {
  const lenCarList = carList.length;
  //handling edge cases
  if (carList == null || lenCarList <= 0) {
    return "No element of car exists.";
  }
  const currYear = new Date().getFullYear();
  if (year > currYear) {
    return "year should be smaller than current year.";
  } else if (year < 0) {
    return "Enter valid year";
  }
  let carOlderYear = carList.filter((car) => car.car_year < year); //creating array for store data of car older than year 2000

  if (carOlderYear.length == 0) {
    return "No Car Exist.";
  } else {
    let details = "";
    //printing the all cars
    for (let i = 0; i < carOlderYear.length; i++) {
      details += `Car ${i + 1}: Brand - ${carOlderYear[i].car_make}, Year - ${
        carOlderYear[i].car_year
      }\n`;
    }
    return `${details}Total cars: ${carOlderYear.length}`;
  }
}

module.exports = getCarOlderByYear;
