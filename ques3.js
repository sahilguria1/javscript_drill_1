//Sorting by model
function sortByModel(carList) {
  const lenCarList = carList.length;
  //handling edge cases
  if (carList == null || lenCarList <= 0) {
    return "No element of car exists.";
  }
  const carModel = carList.map((car) => car.car_make).sort(); // map will retun value in map and the nested function will get car id and the sort will sort by alphabetically.

  //return car model in array format
  return Array.from(carModel);
}

module.exports = sortByModel;
