function getYearOfCar(carList) {
  const lenCarList = carList.length;
  //handling edge cases
  if (carList == null || lenCarList <= 0) {
    return "No element of car exists.";
  }

  const yearOfAllCars = carList.map((car) => car.car_year); //creating array for the year of cars

  return yearOfAllCars; //returning all cars by year
}

module.exports = getYearOfCar;
